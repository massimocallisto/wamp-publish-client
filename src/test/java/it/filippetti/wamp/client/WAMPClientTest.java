package it.filippetti.wamp.client;


import io.crossbar.autobahn.wamp.Session;
import org.junit.Test;


public class WAMPClientTest {

    @Test
    public void testClient() throws Exception {
        try {
            String url = "wss://www.connectanum.com/wamp";
            String realm = "test.unicam.it";

            WAMPClient wampClient = new WAMPClient(url, realm, new OnConnectHandler() {
                @Override
                public void connected(Session session) {
                    session.publish("/test.unicam.it/test", "A message");
                    System.out.println("done pub");
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        Thread.sleep(10000);
        System.out.println("done");
    }

    @Test
    public void testPublish() throws Exception {
        WAMPClient wampClient = null;
        try {
            String url = "wss://www.connectanum.com/wamp";
            String realm = "test.unicam.it";

            wampClient = new WAMPClient(url, realm, null);
        }catch (Exception e){
            e.printStackTrace();
        }

        Thread.sleep(10000);
        if(wampClient != null){
            for(int i = 0; i <  10; i++){
                wampClient.publish("/test.unicam.it/test", "A message "+ i);
                System.out.println("publish: /test.unicam.it/test  --> A message "+ i);
                Thread.sleep(3000);
            }
        }


    }


}