package it.filippetti.wamp.client;

import io.crossbar.autobahn.wamp.Session;

public interface OnConnectHandler {
    void connected(Session session);
}
